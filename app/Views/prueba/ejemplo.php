<?php 
/*******************************************************************************
 * Ejemplo de vista que utiliza la plantilla de adminlte
 * 
 ******************************************************************************/
?>

<?= $this->extend('plantillas/adminlte') ?>

<?= $this->section('page_title') ?>
    <?= $title ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    ¡Hola Mundo!
<?= $this->endSection() ?>
