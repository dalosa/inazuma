<?= $this->extend('plantillas/adminlte') ?>

<?= $this->section('title') ?>
<?= $title ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>


<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inicio - Inazuma Eleven</title>
    <!-- Enlace a Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Enlace a tu archivo CSS personalizado -->
    <link href="styles.css" rel="stylesheet">
    <style>
        .btn-custom {
            background-color: #750d0d;
            color: #fff;
            border: none;
            padding: 10px 20px;
            border-radius: 5px;
            cursor: pointer;
            transition: background-color 0.3s;
        }
        .btn-custom:hover {
            background-color: #ad1111;
        }
        .carousel-item img {
            display: block;
            margin: 0 auto;
        }
        .carousel-item strong {
            display: block;
            text-align: center;
            margin-top: 20px;
            font-size: 18px;
        }
        .header-container {
            background-color: #750d0d;
            color: #fff;
            padding: 20px;
            border-radius: 10px;
            margin-bottom: 20px;
        }
        .main-container {
            background-color: #f8f9fa;
            padding: 30px;
            border-radius: 10px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }
        .description-container {
            background-color: #fff;
            padding: 20px;
            border-radius: 10px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            margin-bottom: 20px;
        }
        .description-container p {
            margin-bottom: 0;
        }
        .description-container img {
            display: block;
            margin: 0 auto;
            width: 80%;
            max-width: 400px;
        }
    </style>
</head>
<body>
    <div class="container">
        <!-- Encabezado -->
        <header class="header-container mt-5">
            <h1 class="text-center">Football Frontier International</h1>
        </header>
        
        <!-- Descripción -->
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="description-container">
                    <p class="text-center">¡Bienvenido a Football Frontier International! Sumérgete en el emocionante mundo de Inazuma Eleven, donde el fútbol se combina con la aventura, la amistad y la competición. Descubre todo sobre los equipos, jugadores y partidos más emocionantes. ¡Únete a nosotros y vive la pasión del fútbol como nunca antes!</p>
                </div>
            </div>
        </div>

        <!-- Imagen -->
        <div class="row justify-content-center">
            <div class="col-md-6 text-center">
                <img src="assets/images/ffi_home.png" class="img-fluid" alt="Football Frontier International">
            </div>
        </div>
        
        <!-- Contenido principal -->
        <main class="main-container mt-5">
            <div class="row">
                <div class="col-md-6">
                    <h2 class="mb-4">Próximos Partidos</h2>
                     
                    <!-- Carrusel de logos de equipos -->
                    <div class="row">
                        <div class="col-md-6">
                            <div id="carouselExampleSlidesOnly" class="carousel slide" data-bs-ride="carousel" data-interval="3000">
                                <div class="carousel-inner">
                                    <div class="carousel-item active">    
                                        <img src="assets/images/escudos/03.png" width="100px">
                                        <strong>vs</strong>
                                        <img src="assets/images/escudos/04.png" width="100px">
                                    </div>
                                    <div class="carousel-item">    
                                        <img src="assets/images/escudos/05.png" width="100px">
                                        <strong>vs</strong>
                                        <img src="assets/images/escudos/01.png" width="100px">
                                    </div>
                                    <div class="carousel-item">    
                                        <img src="assets/images/escudos/07.png" width="100px">
                                        <strong>vs</strong>
                                        <img src="assets/images/escudos/06.png" width="100px">
                                    </div>
                                    <div class="carousel-item">    
                                        <img src="assets/images/escudos/08.png" width="100px">
                                        <strong>vs</strong>
                                        <img src="assets/images/escudos/02.png" width="100px">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center mt-4">
                        <a href="<?php echo site_url('partidos') ?>" class="btn btn-custom">Ver todos los partidos</a>
                    </div>
                </div>
            </div>
        </main>
    </div>

    <!-- Scripts de Bootstrap (jQuery necesario) -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>


<?= $this->endSection() ?>