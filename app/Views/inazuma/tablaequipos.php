<?= $this->extend('plantillas/adminlte') ?>

<?= $this->section('title') ?>
    <?= $title ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>

<div class="container">
        <div class="row justify-content-center">
        
            <div class="col-xl-12">
                    <table id="myTable" class="table rounded border-left border-right border-dark">
                        <thead>
                            <tr>
                                <th class="rounded-top text-white text-center" style="background-color: #750d0d;">Escudo</th>
                                <th class="rounded-top text-white text-center" style="background-color: #750d0d;">Equipo</th>
                                <th class="rounded-top text-white text-center" style="background-color: #750d0d;">Entrenador</th>
                                <th class="rounded-top text-white text-center" style="background-color: #750d0d;">Estadio</th>
                                <th class="rounded-top text-white text-center" style="background-color: #750d0d;">Localidad</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($equipos as $equipo): ?>
                                <tr>
                                    <td class="text-center">
                                        <img src="assets/images/escudos/0<?= $equipo->Cod_equipo ?>.png" width="50px"/>   
                                    </td>
                                    <td class="text-center"><strong><?= $equipo->Nombre ?></strong></td>
                                    <td class="text-center"><strong><?= $entrenadores[$equipo->Entrenador] ?></strong></td>
                                    <td class="text-center"><strong><?= $equipo->Estadio ?></strong></td>
                                    <td class="text-center"><strong><?= $equipo->Localidad ?></strong></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?= $this->endSection() ?>

<?= $this->include('common/datatables') ?>


