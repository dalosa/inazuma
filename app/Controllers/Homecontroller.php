<?php

namespace App\Controllers;
use App\Models\Equipomodel;


class Homecontroller extends BaseController {

    public function index(): string {
        $data['title'] = 'Home';
        return view('inazuma/home', $data);
    }
    
}
