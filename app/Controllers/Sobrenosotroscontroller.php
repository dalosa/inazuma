<?php

namespace App\Controllers;



class Sobrenosotroscontroller extends BaseController {

    public function nosotros(): string {
        $data['title'] = 'Sobre nosotros';
        return view('inazuma/sobrenosotros', $data);
    }

}