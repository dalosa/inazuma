<?php

namespace App\Controllers;

class Home extends BaseController
{
    public function index(): string {
        $data['title'] = 'Esto es un ejemplo';
        return view('welcome_message', $data);
    }
    

    public function adminlte() {
        $data['title'] = 'Esto es un ejemplo';
        return view('plantillas/adminlte', $data);
    }

}
