<?php

namespace App\Controllers;



class Contactanoscontroller extends BaseController {

    public function contacto(): string {
        $data['title'] = 'Sobre nosotros';
        return view('inazuma/contactanos', $data);
    }

}