<?php
namespace app\Models;

use CodeIgniter\Model;

class Equipomodel extends Model{

    protected $table = 'equipo';
    protected $primaryKey = 'Cod_equipo';
    protected $returnType = 'object';
    protected $allowedFields = ['Cod_equipo','Nombre', 'Escudo', 'Localidad', 'Estadio', 'Entrenador'];
}

