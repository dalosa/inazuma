<?php
namespace app\Models;

use CodeIgniter\Model;

class Entrenadormodel extends Model{

    protected $table = 'entrenador';
    protected $primaryKey = 'Cod_entrenador';
    protected $returnType = 'object';
    protected $allowedFields = ['Cod_entrenador','Nombre', 'Apellidos'];
}